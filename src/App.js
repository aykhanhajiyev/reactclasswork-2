import React, {Component} from 'react';
import './App.css';
import {Link, Route} from "react-router-dom";


const User = () => <h1>USER</h1>;
const Login = ()=><h2>LOGIN</h2>
const Register = () =>  <h3>REGISTER</h3>
class App extends Component {
    render() {
        return (
            <div>
                <h1>React router dom</h1>
                <Link to={'/user'}>User</Link>
                <Link to={'/login'}>Login</Link>
                <Link to={'/register'}>Register</Link>

                <Route path={'/user'} component={User}/>
                <Route path={'/login'} component={Login}/>
                <Route path={'/register'} component={Register}/>
            </div>
        );
    }
}

export default App;
